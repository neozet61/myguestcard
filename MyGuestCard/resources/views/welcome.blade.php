<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Guest Book</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN"
            crossorigin="anonymous"></script>
    <style>
        .card {
            margin-top: 20px;
            margin-bottom: 20px;
        }

        form {
            margin-top: 20px;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <!-- record -->
        @foreach($result as $value)
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h6 class="card-subtitle mb-2 text-muted">By {{$value->name}} at {{$value->date}}</h6>
                    <p class="card-text">{{$value->message}}</p>
                    <a href="/edit_form?id={{$value->id}}" class="btn btn-warning">Edit</a>
                    <a href="/delete?id={{$value->id}}" class="btn btn-danger">Delete</a>
                </div>
            </div>
        </div>
        @endforeach
    <div class="row">
        <div class="col-md-6">
            <form method="post" action="/added">
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" name="name" class="form-control" id="name">
                </div>
                <div class="mb-3">
                    <label for="message" class="form-label">Message</label>
                    <textarea class="form-control" name="message" id="message"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Add message</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
