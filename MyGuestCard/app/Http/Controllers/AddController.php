<?php

namespace App\Http\Controllers;

use App\Records;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AddController extends Controller
{
    public function added(Request $request)
    {

        if (isset($_POST['name']) and isset($_POST['message'])) {
            $name = $_POST['name'];
            $message = $_POST['message'];

            $date = date('d.m.Y H:i', time());

            $records = new Records(
                $name,
                $message,
                $date,
            );
            $array = [
                "name" => $records->name,
                "message" => $records->message,
                "date" => $records->date
            ];

            DB::table('wall_on_lara')->insert($array);

        }

        $result = DB::table('wall_on_lara')->get('*');

        return view('welcome', [
            'result' => $result
        ]);
    }
}
