<?php

namespace App\Http\Controllers;

use App\Records;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class EditSaveController extends Controller
{
    public function edit_save(Request $request)
    {

        if (isset($_POST['name']) and isset($_POST['message'])) {
            $name = $_POST['name'];
            $message = $_POST['message'];
            $date = date('d.m.Y H:i', time());
            $id = $_POST['id'];

            $result = DB::table('wall_on_lara')
                ->where('id', '==', $id)
                ->update([
                    'name'=> $name,
                    'message'=> $message,
                    'date'=> $date
                    ]);

            /*  $result = DB::table('wall_on_lara')->get('*');*/

            return redirect('/php')git;
        }
    }
}
