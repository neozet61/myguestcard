<?php

namespace App\Http\Controllers;

use App\Records;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class MainPageController extends Controller
{
    public function show(Request $request)
    {

        $result = DB::table('wall_on_lara')->get('*');

        return view('welcome', [
                'result' => $result
        ]);
    }
}
