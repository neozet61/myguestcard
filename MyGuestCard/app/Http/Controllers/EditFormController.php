<?php

namespace App\Http\Controllers;

use App\Records;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class EditFormController extends Controller
{
    public function edit_form(Request $request)
    {

        if (!isset($_GET['id'])) {
            die("Sorry, but I don't know ID");
        }
        $id = $_GET['id'];
        $result = DB::table('wall_on_lara')
            ->where('id', '=', $id)
            ->get();
        /*  $result = DB::table('wall_on_lara')->get('*');*/

        return view('edit_form', [
            'result' => $result
        ]);
    }
}
