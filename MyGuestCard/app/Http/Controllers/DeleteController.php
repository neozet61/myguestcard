<?php

namespace App\Http\Controllers;

use App\Records;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class DeleteController extends Controller
{
    public function delete(Request $request)
    {

        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            DB::table('wall_on_lara')->delete($id);
        }

        $result = DB::table('wall_on_lara')->get('*');

        return view('welcome', [
            'result' => $result
        ]);
    }
}
