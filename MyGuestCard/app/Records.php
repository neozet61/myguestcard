<?php

namespace App;

class Records
{
    protected int $id;

    public string $name;

    public string $message;

    public string $date;

    /**
     * @param string $name
     * @param int $message
     */
    public function __construct(string $name, string $message, string $date)
    {
        $this->name = $name;
        $this->message = $message;
        $this->date = $date;
    }
}
