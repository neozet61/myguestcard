<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get("/admin", function (){

});


Route::get('/php', [\App\Http\Controllers\MainPageController::class,"show"]);
Route::post('/added', [\App\Http\Controllers\AddController::class,"added"]);
Route::get('/delete', [\App\Http\Controllers\DeleteController::class,"delete"]);
Route::get('/edit_form', [\App\Http\Controllers\EditFormController::class,"edit_form"]);
Route::post('/edit_save', [\App\Http\Controllers\EditSaveController::class,"edit_save"]);
